<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_belongs_to_a_user()
    {
        $this->withoutExceptionHandling();

        $client = factory('App\Model\Client')->create();

        $this->assertInstanceOf('App\User', $client->owner);
    }

    /** @test */
    public function it_has_a_path()
    {
        $this->withoutExceptionHandling();

        $client = factory('App\Model\Client')->create();

        $this->assertEquals('/app/clients/'. $client->id, $client->path());
    }

    /** @test */
    public function it_has_projects()
    {
        $this->withoutExceptionHandling();

        $client = factory('App\Model\Client')->create();

        factory('App\Model\Project', 5)->create(['client_id' => $client->id]);

        $this->assertEquals(5, count($client->project));
    }
}
