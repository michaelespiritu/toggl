<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MembersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_belongs_to_a_user()
    {
        $this->withoutExceptionHandling();

        $member = factory('App\Model\UserMember')->create();

        $this->assertInstanceOf('App\User', $member->owner);
    }

    /** @test */
    public function it_has_a_path()
    {
        $this->withoutExceptionHandling();

        $member = factory('App\Model\UserMember')->create();

        $this->assertEquals('/app/members/'. $member->id, $member->path());
    }

}
