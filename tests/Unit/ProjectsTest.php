<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_belongs_to_a_user()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Model\Project')->create();

        $this->assertInstanceOf('App\User', $project->owner);
    }

    /** @test */
    public function it_has_a_path()
    {
        $this->withoutExceptionHandling();

        $project = factory('App\Model\Project')->create();

        $this->assertEquals('/app/projects/'. $project->id, $project->path());
    }
}
