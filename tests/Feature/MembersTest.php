<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MembersTest extends TestCase
{


    use RefreshDatabase;



    /** @test */
    public function a_user_can_add_member()
    {

        $this->withoutExceptionHandling();

        $user = factory('App\User')->create();

        $this->actingAs($user);

        $anotherUser = factory('App\User')->create();

        $this->post('/app/members', [
            'email' => $anotherUser->email
        ])
            ->assertStatus(200)
            ->assertJsonFragment([ 'success' => 'User has been added' ]);

        $this->assertDatabaseHas('user_members', [
            'member_id' => $anotherUser->id
        ]);
        
    }


    /** @test */
    public function a_user_cannot_same_member_at_the_same_time()
    {

        $this->withoutExceptionHandling();

        $user = factory('App\User')->create();

        $this->actingAs($user);

        $anotherUser = factory('App\User')->create();

        $this->post('/app/members', [
            'email' => $anotherUser->email
        ])
            ->assertStatus(200)
            ->assertJsonFragment([ 'success' => 'User has been added' ]);

        $this->assertDatabaseHas('user_members', [
            'member_id' => $anotherUser->id
        ]);


        $this->post('/app/members', [
            'email' => $anotherUser->email
        ])
            ->assertStatus(302)
            ->assertJsonFragment([ 'error' => 'User is already added to your team.' ]);
        
    }



    /** @test */
    public function a_user_can_delete_member()
    {
        $this->withoutExceptionHandling();

        $user = factory('App\User')->create();

        $this->actingAs($user);

        $userMember = factory('App\Model\UserMember')->create();

        $response = $this->delete($userMember->path());

        $this->assertDatabaseMissing('user_members', $userMember->only('id'));
    }



    /** @test */
    public function a_user_can_view_member()
    {

        $this->withoutExceptionHandling();

        $user = factory('App\User')->create();

        $this->actingAs($user);

        $anotherUser = factory('App\User')->create();

        $this->post('/app/members', [
            'email' => $anotherUser->email
        ]);

        $this->assertDatabaseHas('user_members', [
            'member_id' => $anotherUser->id
        ]);

        $this->get('/app/members')
            ->assertStatus(200);
        
    }


    /** @test */
    public function a_user_cannot_add_member_if_not_registered()
    {

        $this->withoutExceptionHandling();

        $user = factory('App\User')->create();

        $this->actingAs($user);


        $this->post('/app/members', [
            'email' => 'another@email.com'
        ])
            ->assertStatus(302)
            ->assertJsonFragment([ 'error' => 'User not found' ]);
        
    }
}
