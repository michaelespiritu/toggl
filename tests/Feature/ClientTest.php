<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientTest extends TestCase
{


    use RefreshDatabase;



    private function auth_user()
    {
        return $user = factory('App\User')->create();

    }



    /** @test */
    public function a_user_can_add_client()
    {
        $this->withoutExceptionHandling();

        $this->actingAs($this->auth_user());

        $this->post('/app/clients', $attributes = [
            'name' => 'Client Name'
        ])
            ->assertStatus(200)
            ->assertJsonFragment([ 'success' => 'Client has been created']);

        $this->assertDatabaseHas('clients', $attributes);
    }



    /** @test */
    public function a_user_can_edit_client()
    {
        $this->withoutExceptionHandling();

        $this->actingAs($this->auth_user());

        $client = factory('App\Model\Client')->create();
        
        $this->patch($client->path(), [
            'name' => 'New Name'
        ])
            ->assertStatus(200)
            ->assertJsonFragment([ 'success' => 'Client has been updated']);;

        $this->assertDatabaseHas('clients', ['name' => 'New Name']);
    }



    /** @test */
    public function a_user_can_delete_client()
    {
        $this->withoutExceptionHandling();

        $this->actingAs($this->auth_user());

        $client = factory('App\Model\Client')->create();
        
        $this->delete($client->path())
            ->assertJsonFragment([ 'success' => 'Client has been deleted']);;

        $this->assertDatabaseMissing('clients', $client->only('id'));
    }



    /** @test */
    public function a_user_cannot_edit_client_without_name()
    {

        $this->actingAs($this->auth_user());

        $client = factory('App\Model\Client')->create();

        $this->patch($client->path(), [
            'name' => ''
        ])
            ->assertSessionHasErrors('name');
    }



    /** @test */
    public function a_user_cannot_add_client_without_name()
    {

        $this->actingAs($this->auth_user());

        $response = $this->post('/app/clients', [
            'name' => ''
        ]);

        $response->assertSessionHasErrors('name');
    }

    
}
