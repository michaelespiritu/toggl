<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function a_user_can_add_project()
    {
        $this->withoutExceptionHandling();
        
        $user = factory('App\User')->create();

        $this->actingAs($user);

        $this->post('/app/projects', $attributes = [
            'title' => 'New Title'
        ])
            ->assertStatus(200)
            ->assertJsonFragment([ 'success' => 'Project has been created' ]);

        $this->assertDatabaseHas('projects', $attributes);
    }
    

    /** @test */
    public function a_user_can_edit_project()
    {
        $this->withoutExceptionHandling();

        $user = factory('App\User')->create();

        $this->actingAs($user);

        $project = factory('App\Model\Project')->create(['owner_id'=> $user->id]);
        
        $this->patch($project->path(), [
            'title' => 'New Title'
        ])
            ->assertStatus(200)
            ->assertJsonFragment([ 'success' => 'Project has been updated']);;

        $this->assertDatabaseHas('projects', ['title' => 'New Title']);
    }


    /** @test */
    public function a_user_can_delete_client()
    {
        $this->withoutExceptionHandling();

        $this->actingAs(factory('App\User')->create());

        $project = factory('App\Model\Project')->create();
        
        $this->delete($project->path())
            ->assertJsonFragment([ 'success' => 'Project has been deleted']);;

        $this->assertDatabaseMissing('projects', $project->only('id'));
    }


    /** @test */
    public function a_user_can_view_its_own_project()
    {
        $this->withoutExceptionHandling();

        $user = factory('App\User')->create();
        
        $this->actingAs($user);
        
        $project = factory('App\Model\Project')->create(['owner_id'=>$user->id]);

        $this->get($project->path())
            ->assertStatus(200);
    }
    
    
    /** @test */
    public function user_can_add_client_to_project()
    {
        $this->withoutExceptionHandling();

        $user = factory('App\User')->create();
        
        $this->actingAs($user);

        $client = factory('App\Model\Client')->create(['owner_id'=>$user->id]);
        $project = factory('App\Model\Project')->create(['owner_id'=>$user->id]);

        $this->assertDatabaseHas('clients', ['owner_id' => $user->id]);

        $this->post($project->path().'/client', $attributes = [
            'client_id' => $client->id
        ]);

        $this->assertDatabaseHas('projects', $attributes);
    }
    

    /** @test */
    public function user_can_add_member_to_project()
    {

        $this->withoutExceptionHandling();

        $user = factory('App\User')->create();
        
        $this->actingAs($user);

        $project = factory('App\Model\Project')->create(['owner_id' => $user->id]);

        $member = factory('App\Model\UserMember')->create(['owner_id' =>$user->id]);

       $this->post($project->path(). '/member', [
            'user_id' => [$member->member_id]
        ]);

        $this->assertDatabaseHas('project_users', [
            'user_id' => $member->member_id
        ]);
    }


    /** @test */
    public function a_user_cannot_add_project_without_title()
    {
        $user = factory('App\User')->create();

        $this->actingAs($user);

        $this->post('/app/projects', [
            'title' => ''
        ])
            ->assertSessionHasErrors('title');
    }


    /** @test */
    public function guest_cannot_view_projects()
    {
        $this->get('/app/projects')
            ->assertRedirect('/login');
    }
    

    /** @test */
    public function guest_cannot_view_specific_projects()
    {
        $project = factory('App\Model\Project')->create();

        $this->get($project->path())
            ->assertRedirect('/login');
    }


}
