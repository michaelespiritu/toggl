<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProjectsController@index')->middleware('auth');

Route::group(['middleware' => 'auth', 'prefix' => 'app'], function () {
    Route::apiResource('projects', 'ProjectsController');
    Route::post('/projects/{project}/client', 'ProjectsController@addClient');
    Route::post('/projects/{project}/member', 'ProjectsController@addMember');

    Route::apiResource('clients', 'ClientsController');
    Route::apiResource('members', 'MembersController');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
