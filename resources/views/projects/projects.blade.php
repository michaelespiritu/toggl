@extends('layouts.dashboard')

@section('content')
<div class="container py-4 px-4">
            
        <projects :clients="{{auth()->user()->clients}}" :members="{{$members}}"></projects>

</div>
@endsection
