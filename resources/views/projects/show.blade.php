@extends('layouts.dashboard')

@section('content')
<div class="container py-4 px-4">
         
        <show-project :clients="{{auth()->user()->clients}}"></show-project>

</div>
@endsection
