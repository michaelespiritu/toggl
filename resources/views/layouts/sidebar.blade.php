<div class="bg-secondary py-3 px-3 height-100">

    <ul class="mb-0">
        <li class="my-3">
            <a href="/app/clients" class="text-white">Client</a>
        </li>
        <li class="my-3">
            <a href="/app/projects" class="text-white">Projects</a>
        </li>
        <li class="my-3">
            <a href="/app/members" class="text-white">Members</a>
        </li>
    </ul>

</div>