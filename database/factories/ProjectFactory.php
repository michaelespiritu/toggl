<?php

use App\Model\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'owner_id' => factory(App\User::class),
        'title' => $faker->sentence(4)
    ];
});
