<?php

use Faker\Generator as Faker;

$factory->define(App\Model\UserMember::class, function (Faker $faker) {
    return [
        'owner_id' => factory(App\User::class),
        'member_id' => factory(App\User::class)
    ];
});
