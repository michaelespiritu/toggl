<?php

use App\Model\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'owner_id' => factory(App\User::class),
        'name' => $faker->name
    ];
});
