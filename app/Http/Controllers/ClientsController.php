<?php

namespace App\Http\Controllers;

use App\Model\Client;
use Illuminate\Http\Request;
use App\Http\Resources\ClientResource;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($_GET['p'])) {
            return ClientResource::collection(auth()->user()->clients);
        }

        return view('clients.clients');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required'
        ]);
        
        auth()->user()->clients()->create(request([
            'name'
        ]));

        return response([
            'success' => 'Client has been created'
        ], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        if (auth()->user()->isNot($client->owner)) {
            return abort(403);
        }

        if (isset($_GET['p'])) {
            return ClientResource::make($client);
        }

        return view('clients.show');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Client $client)
    {
        request()->validate([
            'name' => 'required'
        ]);

        $client->update(request([
            'name'
        ]));

        return response([
            'success' => 'Client has been updated'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return response([
            'success' => 'Client has been deleted'
        ], 200);
    }
}
