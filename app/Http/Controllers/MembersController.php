<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Resources\MemberResource;
use App\Model\UserMember;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($_GET['p'])) {
            return MemberResource::collection(auth()->user()->members);
        }

        return view('members.members');
    }

    public function findEmailIfExist($email)
    {
        return User::where('email', $email)->first();
    }


    public function findEmailIfExistInTeam($id)
    {
        return UserMember::where('member_id', $id)->first();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $user = $this->findEmailIfExist(request()->email);
        
        if (!$user) {
            return response([
                'error' => 'User not found'
            ], 302);
        }

        if ($this->findEmailIfExistInTeam($user->id)) {
            return response([
                'error' => 'User is already added to your team.'
            ], 302);
        }

        auth()->user()->members()->create([
            'member_id' => $user->id
        ]);

        return response([
            'success' => 'User has been added'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(UserMember $userMember)
    {
        return $userMember;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($userMember)
    {
        UserMember::find($userMember)->delete();

        return response([
            'success' => 'User has been deleted'
        ], 200);
    }
}
