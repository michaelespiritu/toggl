<?php

namespace App\Http\Controllers;

use App\Model\Project;
use App\Model\ProjectUser;
use App\Http\Resources\MemberResource;
use App\Http\Resources\ProjectResource;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($_GET['p'])) {
            return ProjectResource::collection(auth()->user()->accessibleProjects());
        }

        $members = json_encode(MemberResource::collection(auth()->user()->members));

        return view('projects.projects', compact('members'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'title' => 'required'
        ]);
        
        auth()->user()->projects()->create(request([
            'title'
        ]));

        return response([
            'success' => 'Project has been created'
        ], 200);
    }

    /**
     * Store a client in specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addClient(Project $project)
    {
        $project->update(request(['client_id']));

        return response([
            'success' => 'Project has been linked to client.'
        ], 200);
    }

    /**
     * Store a client in specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addMember(Project $project)
    {
        ProjectUser::where('project_id', $project->id)->delete();

        foreach(request('user_id') as $user_id) {

            $project->members()->attach([
                'user_id' => $user_id
            ]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {   
        if (auth()->user()->is($project->owner) || $project->members->contains(auth()->user()) ) {
            if (isset($_GET['p'])) {
                return ProjectResource::make($project);
            }
    
            return view('projects.show');
        }

        return abort(403);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project)
    {
        request()->validate([
            'title' => 'required'
        ]);

        $project->update(request([
            'title'
        ]));

        return response([
            'success' => 'Project has been updated'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();

        return response([
            'success' => 'Project has been deleted'
        ], 200);
    }
}
