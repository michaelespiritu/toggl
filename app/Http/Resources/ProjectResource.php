<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'client' => $this->client,
            'members' => (count($this->members) != 0) ? MemberProjectResource::collection($this->members) : '',
            'path' => $this->path()
        ];
    }
}
