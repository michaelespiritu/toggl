<?php

namespace App;

use App\Model\Project;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Model\UserMember;
use App\Model\ProjectUser;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function projects()
    {
        return $this->hasMany('App\Model\Project', 'owner_id');
    }


    public function clients()
    {
        return $this->hasMany('App\Model\Client', 'owner_id');
    }


    public function members()
    {
        return $this->hasMany('App\Model\UserMember', 'owner_id');
    }


    public function userMember()
    {
        return $this->hasOne('App\Model\UserMember', 'member_id');
    }

    /**
     * Get all projects that the user has access to.
     * 
     * @return mixed
     */
	public function accessibleProjects()
	{
        return Project::where('owner_id', $this->id)
            ->orWhereHas('members', function ($query) {
                $query->where('user_id', $this->id);
            })
            ->get();
	}
}
