<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserMember extends Model
{
    protected $guarded = [];

    
    public function path()
    {
        return "/app/members/{$this->id}";
    }

    public function owner()
    {
        return $this->belongsTo('App\User');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'member_id');
    }
}
