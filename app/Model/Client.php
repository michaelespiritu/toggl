<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = [];

    public function owner()
    {
        return $this->belongsTo('App\User');
    }

    public function project()
    {
        return $this->hasMany('App\Model\Project');
    }

    public function path()
    {
        return "/app/clients/{$this->id}";
    }
}
