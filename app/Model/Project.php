<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{


    protected $guarded = [];


    /**
     *  The path to the project.
     *
     * @return string
     */
    public function path()
    {
        return "/app/projects/{$this->id}";
    }


    /**
     * The owner of the project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\User');
    }


     /**
     * The client of the project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Model\Client');
    }


    /**
     * Get all members that are assigned to the team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany('App\User', 'project_users');
    }

}
