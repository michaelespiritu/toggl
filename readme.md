## Toggl

Laravel, Laravel Mix, VueJs, Boostrap4,  TDD

### Client Section.

* Client can be created by user.
* Client can be assigned to a project.

### Project Section

* Project can be created by user.
* Project can be assigned to a client.
* Project can be assigned to a Member.

### Member Section

* Member can be added by user.
* Member can be assign to a project in Project Page.

run `php artisan migrate --seed` after cloning and setting up.

`npm run install` to install js package
